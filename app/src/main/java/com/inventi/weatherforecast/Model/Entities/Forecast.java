package com.inventi.weatherforecast.model.entities;

import com.google.auto.value.AutoValue;

import java.io.Serializable;

/**
 * Created by filas on 05.11.2016.
 * Entity for one simple forecast
 */

@AutoValue
public abstract class Forecast implements Serializable {

    public abstract String temperature();
    public abstract String weather();
    public abstract String pressure();

    public static Forecast create(String temperature, String weather, String pressure) {
        return new AutoValue_Forecast(temperature, weather, pressure);
    }
}
