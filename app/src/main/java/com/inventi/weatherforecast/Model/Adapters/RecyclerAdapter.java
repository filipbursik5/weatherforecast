package com.inventi.weatherforecast.model.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inventi.weatherforecast.model.entities.Forecast;
import com.inventi.weatherforecast.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by filas on 05.11.2016.
 * Custom adapter for RecyclerView(Future weather)
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private List<Forecast> fore;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView weather, temperature, pressure, date;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            weather = (TextView) view.findViewById(R.id.weatherfuture);
            date = (TextView) view.findViewById(R.id.date);
            temperature = (TextView) view.findViewById(R.id.temperaturefuture);
            pressure = (TextView) view.findViewById(R.id.pressurefuture);
            image = (ImageView) view.findViewById(R.id.imageView4);
        }
    }

    public RecyclerAdapter(List<Forecast> fore) {
        this.fore = fore;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weatheritem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //Set ImageViews and TextViews
        if (fore.size() > 0) {
            holder.weather.setText("Weather: " + fore.get(position).weather());
            holder.temperature.setText("Temperature: " + fore.get(position).temperature());
            holder.pressure.setText("Pressure: " + fore.get(position).pressure());


            switch (fore.get(position).weather()) {
                case "Clear":
                    holder.image.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_sunny));
                    break;
                case "Rain":
                    holder.image.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_cloud));
                    break;
                case "Clouds":
                    holder.image.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_cloud));
                    break;
                case "Snow":
                    holder.image.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.drawable.ic_snowflake));
                    break;
            }
        }

        // Set date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, position + 1);
        String convertedDate = dateFormat.format(cal.getTime());

        holder.date.setText(convertedDate);
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
