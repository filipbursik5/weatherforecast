package com.inventi.weatherforecast.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inventi.weatherforecast.model.adapters.RecyclerAdapter;
import com.inventi.weatherforecast.R;

import butterknife.BindView;

/**
 * Created by filas on 22.10.2016.
 * Fragment for future weather
 */

public class FutureWeatherFragment extends Fragment {

    @BindView(R.id.my_recycler_view) RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_future_weather, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

    }

}
