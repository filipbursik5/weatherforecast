package com.inventi.weatherforecast.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inventi.weatherforecast.R;

import butterknife.BindView;

/**
 * Created by filas on 22.10.2016.
 * Fragment for current weather
 */

public class CurrentWeatherFragment extends Fragment {

    // Bind TextViews
    @BindView(R.id.city) TextView city;
    @BindView(R.id.weather) TextView weather;
    @BindView(R.id.temperature) TextView temperature;
    @BindView(R.id.pressure) TextView pressure;
    @BindView(R.id.humidity) TextView humidity;
    @BindView(R.id.speed) TextView speed;
    @BindView(R.id.deg) TextView deg;

    // Bind ImageViews
    @BindView(R.id.imageView3) ImageView image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_weather, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

    }
}
