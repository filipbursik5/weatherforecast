package com.inventi.weatherforecast.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.inventi.weatherforecast.model.activitymodel.CurrentWeatherModel;
import com.inventi.weatherforecast.model.adapters.ViewPagerAdapter;
import com.inventi.weatherforecast.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Main activity of the app
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Bind views
    @BindView(R.id.view_pager) ViewPager pager;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navView;

    // Variables
    private CurrentWeatherModel mCurrentWeather = new CurrentWeatherModel(getBaseContext());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(toolbar);

        mCurrentWeather.setupPager(pager);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);
        Menu m = navView.getMenu();
        topChannelMenu = m.addSubMenu("Cities");
    }

    /**
     * Overrided onBackPressed, becose of navigation drawer
     */
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        mCurrentWeather.navClicked(item.toString());

        return true;
    }

    /**
     * On click for floating button, shows dialog alert to enter name of the city
     */
    @OnClick(R.id.floating)
    public void onFloating() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Add city");
        dialogBuilder.setMessage("Enter name of city");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
               mCurrentWeather.makeDialog();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    /**
     * On click for delete button, delete all cities from navigation drawer
     */
    @OnClick(R.id.delete)
    public void delete() {
        mCurrentWeather.clearNav();
        Toast.makeText(getApplicationContext(), "All cities from navigation drawer have been deleted!", Toast.LENGTH_SHORT).show();
    }
}
