package com.inventi.weatherforecast.model.activitymodel;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.inventi.weatherforecast.R;
import com.inventi.weatherforecast.model.adapters.ViewPagerAdapter;
import com.inventi.weatherforecast.service.api.CurrentModel;
import com.inventi.weatherforecast.utilities.Requests;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by filas on 12.11.2016.
 */

public class CurrentWeatherModel {

    // Injects
    @Inject CurrentModel mCurrentModel;

    // Binds views
    @BindView(R.id.editText) EditText edt;

    // Variables
    private Context context;
    private SubMenu topChannelMenu;
    private String name;

    // Objects
    private Requests rqs = new Requests();

    // ArrayLists
    private ArrayList<TextView> field = new ArrayList<>();
    private ArrayList<String> list = new ArrayList<>();
    private ArrayList<String> weat;

    public CurrentWeatherModel(Context context) {
        this.context = context;
    }

    public void makeDialog(){

        topChannelMenu.add(edt.getText().toString());
        name = edt.getText().toString();
        //sendRequest(edt.getText().toString());
        list.add(edt.getText().toString());

        try {
            FileOutputStream fout = new FileOutputStream(new File(context.getFilesDir(), "list.wf"));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(list);
            oos.close();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void clearNav() {
        topChannelMenu.clear();
        new File(context.getFilesDir(), "list.wf").delete();
    }

    public void navClicked(String s) {
        name = s;
    }

    public void setupPager(ViewPager pager){
        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (getSupportFragmentManager(), 2, context.getWindow().getDecorView().getRootView());
        pager.setAdapter(adapter);
    }
}
