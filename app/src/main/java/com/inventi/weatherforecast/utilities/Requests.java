package com.inventi.weatherforecast.utilities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.inventi.weatherforecast.model.adapters.RecyclerAdapter;
import com.inventi.weatherforecast.model.entities.Forecast;
import com.inventi.weatherforecast.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by filas on 05.11.2016.
 * Class for sending requests and setting strings to views
 */

public class Requests {

    private ImageView image;
    private String cityName;
    private ArrayList<Forecast> fore = new ArrayList<>();
    private ArrayList<String> weat = new ArrayList<>();
    public Context context;

    /**
     * AsyncTask for send request to weather API
     */
    public class OpenWeatherMapTask extends AsyncTask<Void, Void, String> {

        private Activity act;
        private List<TextView> field = new ArrayList<>();

        private String dummyAppid = "535a568259f16692bdf282bf64ed55c3";
        private String species;
        private int count;
        private RecyclerView view;
        private String queryWeather = "http://api.openweathermap.org/data/2.5/";
        private String queryDummyKey = "&appid=" + dummyAppid;

        private OpenWeatherMapTask(String cityName, Context context, Activity act, RecyclerView view, String species, int count) {
            this(cityName, context, act, species);
            this.view = view;
            this.count = count;
        }

        private OpenWeatherMapTask(String cityName, Context context, Activity act, List<TextView> field, String species) {
            this(cityName, context, act, species);
            this.count = 0;
            this.field = field;
        }

        private OpenWeatherMapTask(String cityName, Context contextt, Activity act, String species) {
            Requests.this.cityName = cityName;
            this.act = act;
            this.species = species;
            context = contextt;
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            String queryReturn;

            String query = null;
            try {
                query = queryWeather + species + URLEncoder.encode(cityName, "UTF-8") + queryDummyKey + (count == 0 ? "" : "&cnt=" + new Integer(count).toString());
                queryReturn = sendQuery(query);
                result += ParseJSON(queryReturn);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e("Logger", "After request");

            if (field.size() > 0 && field.get(0) != null) {
                makeNotification();
                parseString(context, field);
                writeFileCurrent();
            } else if (view != null) {
                RecyclerAdapter mAdapter = new RecyclerAdapter(fore);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                view.setLayoutManager(mLayoutManager);
                view.setItemAnimator(new DefaultItemAnimator());
                view.setAdapter(mAdapter);

                writeFileFuture();
            }
        }

        /**
         * Make notification
         */
        private void makeNotification() {
            if (weat.size() > 0) {
                NotificationCompat.Builder mBuilder =
                        (NotificationCompat.Builder) new NotificationCompat.Builder(act)
                                .setContentTitle("Weather forecast")
                                .setContentText(weat.get(2) + " in " + weat.get(0));

                switch (weat.get(1)) {
                    case "Clouds":
                        mBuilder.setSmallIcon(R.drawable.ic_cloud);
                        break;
                    case "Clear":
                        mBuilder.setSmallIcon(R.drawable.ic_sunny);
                        break;
                    case "Rain":
                        mBuilder.setSmallIcon(R.drawable.ic_cloud);
                        break;
                    case "Snow":
                        mBuilder.setSmallIcon(R.drawable.ic_snowflake);
                        break;
                    default:
                        mBuilder.setSmallIcon(R.drawable.ic_menu_gallery);
                        break;
                }

                NotificationManager mNotificationManager = (NotificationManager) act.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(001, mBuilder.build());
            }
        }

        private String sendQuery(String query) throws IOException {
            String result = "";

            URL searchURL = new URL(query);

            HttpURLConnection httpURLConnection = (HttpURLConnection) searchURL.openConnection();
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(
                        inputStreamReader,
                        8192);

                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }

                bufferedReader.close();
            }

            return result;
        }

        /**
         * Parse jason
         * @param json
         * @return parsed string
         */
        private String ParseJSON(String json) {
            Log.e("Logger", "Parsing");
            String jsonResult = "";
            weat.clear();
            if (count > 0) {
                ParseJSONDays(json);
            } else {
                try {
                    JSONObject JsonObject = new JSONObject(json);
                    String cod = jsonHelperGetString(JsonObject, "cod");
                    if (cod != null) {
                        if (cod.equals("200")) {
                            weat.add(jsonHelperGetString(JsonObject, "name"));
                            JSONArray weather = jsonHelperGetJSONArray(JsonObject, "weather");
                            if (weather != null) {
                                for (int i = 0; i < weather.length(); i++) {
                                    JSONObject thisWeather = weather.getJSONObject(i);
                                    weat.add(jsonHelperGetString(thisWeather, "main"));
                                }
                            }

                            JSONObject main = jsonHelperGetJSONObject(JsonObject, "main");
                            if (main != null) {
                                weat.add("Temperature: " + new Double(Math.round(Double.parseDouble(jsonHelperGetString(main, "temp")) - 272)).toString());
                                weat.add("Pressure: " + jsonHelperGetString(main, "pressure"));
                                weat.add("Humidity: " + jsonHelperGetString(main, "humidity"));
                            }

                            JSONObject wind = jsonHelperGetJSONObject(JsonObject, "wind");
                            if (wind != null) {
                                weat.add("Speed: " + jsonHelperGetString(wind, "speed"));
                                weat.add("Degress: " + jsonHelperGetString(wind, "deg"));
                            }

                        } else if (cod.equals("404")) {
                            String message = jsonHelperGetString(JsonObject, "message");
                            jsonResult += "cod 404: " + message;
                        }
                    } else {
                        jsonResult += "cod == null\n";
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    jsonResult += e.getMessage();
                }
            }
            return jsonResult;
        }

        /**
         * Parse jason
         * @param json
         * @return parsed string
         */
        private void ParseJSONDays(String json) {
            try {
                JSONObject JsonObject = new JSONObject(json);
                String cod = jsonHelperGetString(JsonObject, "cod");
                fore.clear();
                if (cod != null) {
                    if (cod.equals("200")) {
                        for (int i = 0; i < count; i++) {
                            JSONArray list = jsonHelperGetJSONArray(JsonObject, "list");
                            if (list != null) {
                                JSONObject thisWeather = list.getJSONObject(i);
                                fore.add(Forecast.create(new Double(Math.round(Double.parseDouble(jsonHelperGetString(jsonHelperGetJSONObject(thisWeather, "temp"), "day")) - 272)).toString(),
                                        jsonHelperGetString(jsonHelperGetJSONArray(thisWeather, "weather").getJSONObject(0), "main"),
                                        jsonHelperGetString(thisWeather, "pressure").toString()));
                            }
                        }
                    }
                } else if (cod.equals("404")) {
                    String message = jsonHelperGetString(JsonObject, "message");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private String jsonHelperGetString(JSONObject obj, String k) {
            String v = null;
            try {
                v = obj.getString(k);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return v;
        }

        private JSONObject jsonHelperGetJSONObject(JSONObject obj, String k) {
            JSONObject o = null;

            try {
                o = obj.getJSONObject(k);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return o;
        }

        private JSONArray jsonHelperGetJSONArray(JSONObject obj, String k) {
            JSONArray a = null;

            try {
                a = obj.getJSONArray(k);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return a;
        }

    }

    /**
     * Method for write current weather as Arraylist<String> to file
     */
    private void writeFileCurrent() {
        try {
            FileOutputStream fout = new FileOutputStream(new File(context.getFilesDir(), "currentweather.wf"));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(weat);
            oos.close();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method for write future weather as ArrayList<Forecast> to file
     */
    private void writeFileFuture() {
        try {
            FileOutputStream fout = new FileOutputStream(new File(context.getFilesDir(), "futureweather.wf"));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(fore);
            oos.close();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Strings to TextViews and ImageView
     * @param context
     * @param field list of TextViews
     */
    private void parseString(Context context, List<TextView> field) {
        if (weat.size() > 0) {
            for (int i = 0; i < field.size(); i++) {
                if (field.get(i) != null) {
                    field.get(i).setText(weat.get(i));
                }
            }
            switch (weat.get(1)) {
                case "Clear":
                    image.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_sunny));
                    break;
                case "Rain":
                    image.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_cloud));
                    break;
                case "Clouds":
                    image.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_cloud));
                    break;
                case "Snow":
                    image.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_snowflake));
                    break;
            }
        }
    }

    /**
     * Method for send request to weather API
     * @param cityName name of city
     * @param context
     * @param act Activity
     * @param field list of TextViews
     * @param image ImageView
     */
    public void sendRequest(String cityName, Context context, Activity act, List<TextView> field, ImageView image) {
        this.image = image;
        new Requests.OpenWeatherMapTask(
                cityName, context, act, field, "weather?q=").execute();
    }

    /**
     * Method for send request to weather API
     * @param cityName name of city
     * @param context
     * @param act Activity
     * @param view Recycler view
     * @param count count of days
     */
    public void sendRequest(String cityName, Context context, Activity act, RecyclerView view, int count) {
        new Requests.OpenWeatherMapTask(
                cityName, context, act, view, "forecast/daily?q=", count).execute();
    }

}
